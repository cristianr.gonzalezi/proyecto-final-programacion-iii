package proyectoPalindromo.hilos;

import java.util.logging.Level;
import java.util.logging.Logger;
import proyectoPalindromo.model.CrearArchivo;

public class HiloEscribir extends Thread{

    private String rutaEscribir;
    private String contenido;
    private String texto;

    public HiloEscribir(String rutaEscribir, String contenido,String texto){
        this.rutaEscribir = rutaEscribir;
        this.contenido = contenido;
        this.texto= texto;
    }
    
    @Override
    public void run() {
        try {
            System.out.println("Ejecutando Hilo Escribir...");
            CrearArchivo.archivo(this.rutaEscribir, this.contenido, this.texto);
            System.out.println("Se ha escrito en el archivo...");
        } catch (Exception ex) {
            ex.printStackTrace(System.err);
            System.out.println("Error al escribir en el archivo...");
        }
    }
}
