package proyectoPalindromo.hilos;

import proyectoPalindromo.model.ProyectoPalindroma;

public class HiloPalabra extends Thread{
    public String palabra;
    private String texto;
    private int pos;

    public HiloPalabra(String texto, int pos){
        this.texto = texto;
        this.pos = pos;
    }
    
    @Override
    public void run() {
        try {
            System.out.println("Ejecutando Hilo Sacar Palabra...");
            this.palabra = ProyectoPalindroma.palabraAPabara(this.texto, this.pos, "");
            HiloPalabra.sleep(2000);
        } catch (InterruptedException ex) {
            System.out.println("Error al sacar la palabra...");
        }
    }

    public String getPalabra() {
        return palabra;
    }
}