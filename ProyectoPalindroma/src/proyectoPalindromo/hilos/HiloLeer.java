package proyectoPalindromo.hilos;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import proyectoPalindromo.model.LeerTexto;

public class HiloLeer extends Thread{
    
    public String resultado;
    private String rutaLeer;
    
    public HiloLeer(String rutaLeer){
        this.rutaLeer = rutaLeer;
    }
    
    @Override
    public void run() {
        try {
            System.out.println("Ejecutando Hilo leer archivo...");
            BufferedReader entrada = new BufferedReader(new FileReader(this.rutaLeer));
            this.resultado = LeerTexto.leer("", entrada, "");
            System.out.println("Se ha leído el contenido del archivo...");
        } catch (IOException ex) {
            ex.printStackTrace(System.err);
            System.out.println("Error al leer el archivo...");
        }
    }

    public String getResultado() {
        return resultado;
    }
}
