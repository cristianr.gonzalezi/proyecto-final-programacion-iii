package proyectoPalindromo.controller;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.text.DefaultCaret;
import proyectoPalindromo.model.ProyectoPalindroma;
import proyectoPalindromo.Exception.ExceptionPersonalizada;
import proyectoPalindromo.hilos.*;
import proyectoPalindromo.view.Interfaz;

public class Controller {
    public void cargar(){
        JFileChooser ruta = new JFileChooser();
        ruta.setFileSelectionMode(JFileChooser.FILES_ONLY);
        ruta.setAcceptAllFileFilterUsed(false);
        ruta.setFileFilter(new FileNameExtensionFilter("TXT", "txt"));
        ruta.showOpenDialog(null);
        try {
            String rutaSeleccionada = ruta.getSelectedFile().getPath();
            HiloLeer leer = new HiloLeer(rutaSeleccionada);
            leer.start();
            leer.join();
            Interfaz.jTextArea1.setText(leer.getResultado());
        } catch (NullPointerException ex) {
            if (Interfaz.jTextArea1.getText().equals("")) {
                Interfaz.jTextArea1.setText("No se ha seleccionado ningún archivo");
            }
        } catch (InterruptedException ex) {
            JOptionPane.showMessageDialog(null, "Se ha interrumpido el proceso de lectura del archivo...");
        }
    }
    
    public void guardar(){
        JFileChooser f = new JFileChooser();
        f.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
        f.showSaveDialog(null);
        String ruta = f.getSelectedFile().getPath();
        if(ruta.contains(".txt")){
        try {
           
            HiloEscribir escribir = new HiloEscribir(ruta, Interfaz.jTextArea2.getText(),Interfaz.jTextArea1.getText());
            escribir.start();
            escribir.join();
            JOptionPane.showMessageDialog(null, "Se ha escrito en el archivo: " + ruta);
        } catch (NullPointerException ex) {
            if (Interfaz.jTextArea2.getText().equals("")) {
                Interfaz.jTextArea2.setText("No se ha seleccionado ningún archivo");
            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
        }
    }else{
            String nombreArchivo=JOptionPane.showInputDialog("ingrese nombre de archivo");
            try {
            if(nombreArchivo.equals("")){
            JOptionPane.showMessageDialog(null, "ingrese un nombre de archivo");
               
                guardar();
            }
            else
            ruta+="\\"+nombreArchivo+".txt";
       
               HiloEscribir escribir = new HiloEscribir(ruta, Interfaz.jTextArea2.getText(),Interfaz.jTextArea1.getText());
            escribir.start();
            escribir.join();
            JOptionPane.showMessageDialog(null, "Se ha escrito en el archivo: " + ruta);
        
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, "Ruta invalida, no se encontro");
       }
    
        }
            
        }
    
    public void iniciarRecorrido(){
        try {
            Interfaz.jTextArea2.setText("");
            Interfaz.jTextField1.setText("");
            Interfaz.jTextField2.setText("");
            Interfaz.jTextField3.setText("");
            String texto = Interfaz.jTextArea1.getText();
            ProyectoPalindroma.inicio(texto, 0, "", "");
            
        } catch (InterruptedException ex) {
            ex.printStackTrace(System.err);
            System.out.println("Hilo interrumpido...");
        }
    }
}