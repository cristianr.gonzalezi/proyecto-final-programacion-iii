/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package proyectoPalindromo.Exception;

import java.io.IOException;

/**
 *
 * @author crist
 */
public class ExceptionPersonalizada extends IOException{
     public ExceptionPersonalizada(String texto) {
      super(texto);
     }
}
