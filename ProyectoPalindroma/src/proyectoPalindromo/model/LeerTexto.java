/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package proyectoPalindromo.model;

import java.io.BufferedReader;

import java.io.IOException;

/**
 *
 * @author crist
 */
public class LeerTexto {

    public static String leer(String resultado, BufferedReader obj, String texto) throws IOException {
        if ((texto = obj.readLine()) == null) {
            return resultado;
        } else {
            resultado += texto + "\n";
        }

        return leer(resultado, obj, texto);
    }
}
