/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoPalindromo.model;



import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import javax.swing.JOptionPane;

public class CrearArchivo {

    public static void archivo(String ruta, String contenido, String textoOriginal) throws Exception {
        try {
            File file = new File(ruta);
            // Si el archivo no existe es creado
            if (!file.exists()) {
                file.createNewFile();
            }
            FileWriter fw = new FileWriter(file);
           try (BufferedWriter bw = new BufferedWriter(fw)) {
                bw.write("TEXTO ORIGINAL:"+"\n"+
                        textoOriginal
                        +"\n"
                                + "\n"
                                + "\n"
                                + "\n"
                                + "TEXTO MODIFICADO:"
                                + "\n"+contenido);
                System.out.println("Done writing");
            }
        } catch (FileNotFoundException e) {
            throw new Exception("Ruta invalida: No se encontro el Archivo.");
        } catch (IOException e) {
            JOptionPane.showMessageDialog(null, "Ruta vacia");
            //e.printStackTrace();
        } catch (Exception e) {
            System.out.println("Error inesperado");
        }
    }

}
