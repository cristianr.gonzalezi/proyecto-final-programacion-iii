package proyectoPalindromo.model;

import javax.swing.text.DefaultCaret;
import proyectoPalindromo.hilos.HiloPalabra;
import proyectoPalindromo.main.Main;
import proyectoPalindromo.view.Interfaz;

public class ProyectoPalindroma {

  
    public static String inicio(String texto, int i, String palabra, String resultado) throws InterruptedException {
        if (i >= texto.length()) {
            return resultado;
        } else {
            if (texto.charAt(i) == ' ') {
                return inicio(texto, i + 1, palabra, resultado);
            } else {
                HiloPalabra hiloPalabra = new HiloPalabra(texto, i);
                hiloPalabra.start();
                hiloPalabra.join(2000);
                palabra = hiloPalabra.getPalabra();
                System.out.println("Palabra: " + palabra);
                if (!palabra.equals("")) {
                    Interfaz.jTextField1.setText(palabra);
                }
                if (i > 0) {
                    if (texto.charAt(i - 1) == '\n') {
                        resultado += "\n";
                        Interfaz.jTextArea2.append("\n");
                    }
                    if (texto.charAt(i - 1) == '\t') {
                        resultado += "\t";
                        Interfaz.jTextArea2.append("\t");
                    }
                }
                if (resultado.length() > 0 && i > 0 && ((texto.charAt(i) >= 97 && texto.charAt(i) <= 122)
                        || (texto.charAt(i) >= 65 && texto.charAt(i) <= 90) || (texto.charAt(i) == 'á'
                        || texto.charAt(i) == 'é' || texto.charAt(i) == 'í' || texto.charAt(i) == 'ó'
                        || texto.charAt(i) == 'ú'))) {
                    if (texto.charAt(i - 1) == ' ' || (texto.charAt(i - 1) >= 33 && texto.charAt(i - 1) <= 64)) {
                        resultado += " ";
                        Interfaz.jTextArea2.append(" ");
                    }
                }//fin if
                System.out.println("Tamaño: " + palabra.length());
                if (palabra.length() >= 4) {
                    if (ProyectoPalindroma.isPalindroma(palabra, 0, palabra.length() - 1, true)) {
                        resultado += palabra;
                        Interfaz.jTextField2.setText("Es palindroma...");
                        Interfaz.jTextArea2.append(palabra);
                    } else {
                        Interfaz.jTextField2.setText("No es palindroma...");
                        if (ProyectoPalindroma.isConvertible(palabra, 0, true, 0)) {
                            Interfaz.jTextField2.setText(Interfaz.jTextField2.getText() + " Es convertible...");
                            String[] nuevaPalabra = ProyectoPalindroma.convertir(palabra, 0, new String[palabra.length()], 0, ProyectoPalindroma.contCaracter(palabra, 0, 0, palabra.charAt(0)));
                            resultado = forPalabra(0, nuevaPalabra, resultado);
                            Interfaz.jTextField3.setText(forPalabra(0, nuevaPalabra, ""));
                            Interfaz.jTextArea2.append(forPalabra(0, nuevaPalabra, ""));
                        } else {
                            resultado += palabra;
                            Interfaz.jTextField2.setText(Interfaz.jTextField2.getText() + " No es convertible...");
                            Interfaz.jTextField3.setText(palabra);
                            Interfaz.jTextArea2.append(palabra);
                        }
                    }
                } else {
                    resultado += palabra;
                    Interfaz.jTextArea2.append(palabra);
                    Interfaz.jTextField2.setText("Palabra muy corta para convertir...");
                    Interfaz.jTextField3.setText(palabra);
                }
                i += palabra.length();
            }
            Interfaz.jTextArea2.update(Interfaz.jTextArea2.getGraphics());
            Interfaz.jTextField1.update(Interfaz.jTextField1.getGraphics());
            Interfaz.jTextField2.update(Interfaz.jTextField2.getGraphics());
            Interfaz.jTextField3.update(Interfaz.jTextField3.getGraphics());
            return inicio(texto, i + 1, palabra, resultado);
        }
    }

    private static String forPalabra(int j, String[] nuevaPalabra, String resultado) {
        if (j > nuevaPalabra.length - 1) {
            return resultado;
        } else {
            resultado += nuevaPalabra[j];
            return forPalabra(j + 1, nuevaPalabra, resultado);
        }
    }

    public static boolean isPalindroma(String palabra, int cont1, int cont2, boolean palindroma) {
        if ((cont1 == palabra.length() && cont2 == -1) || palindroma == false) {
            return palindroma;
        } else {
            if (palabra.charAt(cont1) == palabra.charAt(cont2)) { //compara el primer caracter con el ultimo
                return isPalindroma(palabra, cont1 + 1, cont2 - 1, true);
            } else {
                return isPalindroma(palabra, cont1, cont2, false);
            }
        }
    }

    public static String palabraAPabara(String texto, int pos, String palabra) {

        String toLowerCase = texto.toLowerCase();

        if (pos == toLowerCase.length() || (toLowerCase.charAt(pos) >= 33 && toLowerCase.charAt(pos) <= 64)
                || toLowerCase.charAt(pos) == '\n' || toLowerCase.charAt(pos) == ' ' || toLowerCase.charAt(pos) == '{'
                || toLowerCase.charAt(pos) == '}' || toLowerCase.charAt(pos) == '\t') {
            return palabra;
        } else {
            switch (toLowerCase.charAt(pos)) {
                case 'á':
                    palabra += 'a';
                    break;

                case 'é':
                    palabra += 'e';
                    break;

                case 'í':
                    palabra += 'i';
                    break;

                case 'ó':
                    palabra += 'o';
                    break;

                case 'ú':
                    palabra += 'u';
                    break;

                default:
                    palabra += texto.charAt(pos);
                    break;
            }
        }
        return palabraAPabara(texto, pos + 1, palabra);

    }

    public static boolean isConvertible(String palabra, int cont, boolean convertible, int cont2) {
        if ((cont == palabra.length()) || convertible == false) {
            return convertible;
        } else {
            char letra = palabra.charAt(cont);
            if (!palabra.substring(0, cont).contains(String.valueOf(letra))) {
                if (palabra.length() % 2 == 0) { // determina si la palabra es de longitud par 
                    if (contCaracter(palabra, 0, 0, letra) % 2 == 0) { // determina si el caracter esta un numero par de veces
                        return isConvertible(palabra, cont + 1, true, cont2);
                    } else {
                        return isConvertible(palabra, cont, false, cont2);
                    }
                } else {
                    if (contCaracter(palabra, 0, 0, letra) % 2 == 0) { // determina si el caracter esta un numero par de veces
                        return isConvertible(palabra, cont + 1, true, cont2);
                    } else {
                        if (cont2 > 1) { // determina la cantidad de veces que hay un caracter impar
                            return isConvertible(palabra, cont, false, cont2);
                        } else {
                            return isConvertible(palabra, cont + 1, true, cont2 + 1);
                        }

                    }
                }
            } else {
                return isConvertible(palabra, cont + 1, true, cont2);
            }
        }
    }

    public static int contCaracter(String palabra, int cont, int cont2, char letra) {
        if (cont == palabra.length()) {
            return cont2;
        } else {
            if (letra == palabra.charAt(cont)) { // compara la letra con cada caracter en la posicion cont
                return contCaracter(palabra, cont + 1, cont2 + 1, letra);
            } else {
                return contCaracter(palabra, cont + 1, cont2, letra);
            }
        }
    }

    public static String[] convertir(String palabra, int cont, String[] resultado, int cont2, int cantLetra) {
        if (cont == palabra.length() || cont2 == resultado.length) {
            return resultado;
        } else {
            if (cantLetra == 0) { // verifica si ya esta el/los caracteres dentro del arreglo
                if (cont <= palabra.length() - 2) {
                    cantLetra = contCaracter(palabra, 0, 0, palabra.charAt(cont + 1));
                }
                return convertir(palabra, cont + 1, resultado, cont2, cantLetra);
            } else {
                if (resultado[cont2] == null && !palabra.substring(0, cont).contains(String.valueOf(palabra.charAt(cont)))) { //comprueba si el arreglo en esa posición esta vacio y si el caracter que se esta evaluando ya se ha posicionado antes
                    if (cantLetra % 2 == 0) { //comprueba si la cantidad de letras es par o impar
                        resultado[cont2] = String.valueOf(palabra.charAt(cont));
                        resultado[resultado.length - (cont2 + 1)] = String.valueOf(palabra.charAt(cont));
                        return convertir(palabra, cont, resultado, cont2 + 1, cantLetra - 2);
                    } else {
                        resultado[resultado.length / 2] = String.valueOf(palabra.charAt(cont));
                        return convertir(palabra, cont, resultado, (resultado.length / 2) + 1, cantLetra - 1);
                    }
                } else {
                    return convertir(palabra, cont, resultado, cont2, 0);
                }

            }
        }
    }
}
